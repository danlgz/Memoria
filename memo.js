let isPlaying = false
        let history = []
        let level = 0
        const btn = document.getElementById('toggle')
        const view = document.getElementById('view')

        document.addEventListener('keyup', (event) => {
            if (isPlaying) {
                if (history[level] == event.keyCode) {
                    activate(event.keyCode, { success: true })
                    view.textContent = `${level + 1} / ${history.length}`
                    level++
                    setTimeout(() => {
                        if (history[history.length - 1] == event.keyCode) {
                            swal("Bien! pasa el siguiente nivel", "", "success")
                                .then(() => {
                                    setTimeout(() => {
                                        level = 0
                                        let key
                                        validator = false
                                        while (!validator) {
                                            key = randomKey()
                                            if ((history.findIndex(ob => ob == key)) == -1) {
                                                history.push(key)
                                                validator = true
                                            }
                                        }
                                        let count = 0
                                        const interval = setInterval(() => {
                                            activate(history[count])
                                            view.textContent = `0 / ${history.length}`
                                            if (history[history.length - 1] == history[count]) clearInterval(interval)
                                            count++
                                        }, 600)
                                    }, 300)
                                })
                        }
                    }, 500)
                } else {
                    activate(event.keyCode, { fail: true })
                    finishGame()
                    setTimeout(() => {
                        swal("Has perdido :(", "", "error")
                            .then(() => view.textContent = `0 / ${history.length}`)
                    }, 500)
                }
            }
        })

        function toggle() {
            if (!isPlaying) {
                btn.textContent = 'Terminar'
                const key = randomKey()
                activate(key)
                history.push(key)
                view.textContent = `0 / ${history.length}`
            } else {
                finishGame()
            }
            isPlaying = !isPlaying
        }

        function finishGame() {
            history = []
            btn.textContent = 'Jugar'
            level = 0
        }

        function randomKey() {
            const max = 90
            const min = 65
            return Math.round(Math.random() * (max - min) + min)
        }

        function getElementByIdCode(keyCode) {
            return document.querySelector(`[data-key="${keyCode}"]`)
        }

        function activate(keyCode, opts = {}) {
            addClass(keyCode, 'active')
            addClass(keyCode, opts.success ? 'success' : '')
            addClass(keyCode, opts.fail ? 'fail' : '')
        }

        function addClass(el, className) {
            if (className != '') {
                getElementByIdCode(el).classList.add(className)
                setTimeout(() => getElementByIdCode(el).classList.remove('activate', className), 600)
            }
        }